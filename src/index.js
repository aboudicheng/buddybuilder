import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import AOS from 'aos';
import 'url-search-params-polyfill';
import store from "./store";
import './index.scss';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';

AOS.init();

ReactDOM.render(
    <Provider store={store}>
            <App />
    </Provider>,
    document.getElementById('root'));

registerServiceWorker();
