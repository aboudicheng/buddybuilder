import * as actions from '../constants/action_types';

export function onSetAuthUser(authUser) {
    return {
        type: actions.AUTH_USER_SET,
        authUser
    }
}