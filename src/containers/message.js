import React, { Component } from 'react';
import {
    Card,
} from 'antd';

class Message extends Component {

    render() {
        const flexStyle = {
            display: "flex",
            flexFlow: "row wrap",
            justifyContent: "center"
        }

        return (
            <div>
                    <Card
                        className="pswforget-box"
                        data-aos="fade-down"
                        data-aos-duration="1000"
                        data-aos-once="true"
                        bodyStyle={flexStyle}
                    >
                        <div className="middle">
                            <h1>{this.props.title}</h1>
                            <p>{this.props.message}</p>
                        </div>

                    </Card>
            </div>
        );
    }
}

export default Message;