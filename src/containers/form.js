import React from 'react';
import {
    Button,
    Radio,
    DatePicker,
    Select,
    Form,
    Tooltip
} from 'antd';

const FormItem = Form.Item;

function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

const FormContainer = (props) => (
    <>
        <FormItem label="Sex:">
            {props.getFieldDecorator('sex', { rules: [{ required: true }], initialValue: 'M' })(
                <Radio.Group>
                    <Radio value='M' size="large">Male</Radio>
                    <Radio value='F' size="large">Female</Radio>
                </Radio.Group>
            )}
        </FormItem>
        <FormItem label="Birth Date:">
            {props.getFieldDecorator('birthdate', {
                rules: [{ type: 'object', required: true, message: 'Please select time!' }],
                initialValue: null
            })(
                <DatePicker size="large" />
            )}
        </FormItem>
        <FormItem
            validateStatus={props.locationError ? 'error' : ''}
            help={''}
            label="Country"
        >
            {props.getFieldDecorator('location', {
                rules: [{ required: true, message: 'Please input your country!' }],
                initialValue: ''
            })(
                <Tooltip placement="top" title={props.locationError} trigger="focus">
                    <Select
                        style={{ width: 190 }}
                        onChange={value => { props.handleChange('location')(value); props.setFieldsValue({ region: props.getRegions(value)[0] }) }}
                        size="large"
                    >
                        {props.countries.length !== 0 && props.countries.map(country => <Select.Option key={country}>{country}</Select.Option>)}
                    </Select>
                </Tooltip>
            )}
        </FormItem>
        <FormItem
            validateStatus={props.regionError ? 'error' : ''}
            help={''}
            label="Region:"
        >
            {props.getFieldDecorator('region', {
                rules: [{ required: true, message: 'Please input your region!' }],
                initialValue: ''
            })(
                <Tooltip placement="top" title={props.regionError} trigger="focus">
                    <Select
                        style={{ width: 190 }}
                        onChange={props.handleChange('region')}
                        size="large"
                    >
                        {!!props.getFieldValue('location') && props.getRegions(props.getFieldValue('location')).map(city => <Select.Option key={city}>{city}</Select.Option>)}
                    </Select>
                </Tooltip>
            )}
        </FormItem>
        <div className="signup-btn">
            <Button
                style={{ backgroundColor: "#16a085", color: "#DDD" }}
                size="large"
                htmlType="submit"
                disabled={hasErrors(props.getFieldsError())}
                loading={props.loading}
            >Sign me up</Button>
        </div>
    </>
)

export default FormContainer;