import { combineReducers } from "redux";
import sessionReducer from './session';
import timerReducer from './timer';

const rootReducer = combineReducers({
    sessionState: sessionReducer,
    timerState: timerReducer
})

export default rootReducer;