import * as actions from '../constants/action_types';

const INITIAL_STATE = {
    status: "STOP",
}

export function timerReducer(state = INITIAL_STATE, action) {
    switch(action.type) {
        case actions.TIMER_START:
            return { ...state, status: "START" };
        case actions.TIMER_STOP:
            return { ...state, status: "STOP" };
        default: return state;
    }
}

export default timerReducer;