import React, { useState, useEffect } from 'react';
import { compose } from 'recompose';
import { withRouter } from 'react-router';
import {
    Input,
    Button,
    Icon,
    Card,
    Form,
    Tooltip,
    message
} from 'antd';
import axios from 'axios';
import * as domains from '../../constants/api_endpoints';
import * as validators from '../../helpers/formValidation';
import * as routes from '../../constants/routes';

const FormItem = Form.Item;

function Reset(props) {
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        document.title = 'Reset Password';
    })

    function submit(e) {
        e.preventDefault();
        setLoading(true);
        props.form.validateFields(
            async (err) => {
                if (!err) {
                    const { password, confirm } = props.form.getFieldsValue();
                    const { uid, secret_token } = props.match.params;
                    try {
                        const requestBody = {
                            new: password,
                            verify: confirm,
                        }
                        const { data } = await axios.post(`${domains.host}/api/reset/${uid}/${secret_token}`, requestBody, {
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                        })

                        if (!!data.error) {
                            setLoading(false);
                            message.error(data.error);
                        }
                        else {
                            setLoading(false);
                            props.history.push(routes.LOGIN);
                        }
                    }
                    catch (error) {
                        setLoading(false);
                        console.log(error)
                    }
                }
                else {
                    setLoading(false);
                    console.log(err);
                }
            }
        )
    }

    const {
        getFieldDecorator,
        getFieldError,
        getFieldValue
    } = props.form;

    const passwordError = getFieldError('password');
    const confirmError = getFieldError('confirm');

    const flexStyle = {
        display: "flex",
        flexFlow: "row wrap",
        justifyContent: "center"
    }

    return (
        <Form onSubmit={submit}>
            <Card
                className="pswforget-box"
                data-aos="fade-down"
                data-aos-duration="1000"
                data-aos-once="true"
                bodyStyle={flexStyle}
            >
                <div className="middle">
                    <h1>Reset Password</h1>
                    <p>Please enter your new password.</p>

                    <FormItem
                        validateStatus={passwordError ? 'error' : ''}
                        help={''}
                    >
                        {getFieldDecorator('password', {
                            validateTrigger: 'onBlur',
                            rules: [
                                { validator: validators.validateToNextPassword }
                            ],
                            initialValue: ''
                        })(
                            <Tooltip placement="top" title={passwordError} trigger="focus">
                                <Input.Password
                                    size="large"
                                    type="password"
                                    placeholder="Password"
                                    prefix={<Icon type="lock" />}
                                    required
                                />
                            </Tooltip>
                        )}
                    </FormItem>
                    <FormItem
                        validateStatus={confirmError ? 'error' : ''}
                        help={''}
                    >
                        {getFieldDecorator('confirm', {
                            rules: [
                                { validator: validators.compareToFirstPassword(getFieldValue('password')) }
                            ],
                            initialValue: ''
                        })(
                            <Tooltip placement="top" title={confirmError} trigger="focus">
                                <Input.Password
                                    size="large"
                                    type="password"
                                    placeholder="Confirm Password"
                                    prefix={<Icon type="lock" />}
                                    onChange={e => props.form.setFieldsValue({ confirm: e.target.value })}
                                    required
                                />
                            </Tooltip>
                        )}
                    </FormItem>

                    <Button
                        style={{ backgroundColor: "#16a085", color: "#DDD" }}
                        size="large"
                        htmlType="submit"
                        loading={loading}
                    >Send</Button>
                </div>

            </Card>
        </Form>
    )
}

export default compose(
    withRouter,
    Form.create()
)(Reset);