import React, { Component } from 'react';
import {
    Avatar
} from 'antd';
import './profile.scss';

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div className="profile-page">
                <div className="profile-bg"></div>
                <div className="profile-pic"><Avatar icon="user" size={128} /></div>
                
            </div>
        );
    }
}

export default Profile;