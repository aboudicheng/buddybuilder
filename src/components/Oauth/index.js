import React, { Component } from 'react';
import { compose } from 'recompose';
import {
    Card,
    Form
} from 'antd';
import { withRouter } from "react-router";
import _ from 'lodash';
import FormContainer from '../../containers/form';
import * as domains from '../../constants/api_endpoints';
import * as routes from '../../constants/routes';

class Oauth extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            countries: [],
            loading: false,
        }
    }

    async componentDidMount() {
        document.title = 'Personal Information'
        //Make sure the user accesses here by clicking OAuth button
        if (!this.props.location.state || this.props.location.state.error) {
            this.props.history.push(routes.HOME);
        }
        else {
            //get countries & regions data
            const res = await fetch(`${process.env.PUBLIC_URL}/data.json`);
            const json = await res.json();

            let countries = [];
            json.forEach(n => {
                countries.push(n.countryName);
            })
            this.setState({ data: json, countries });
        }
    }

    /**
    * @param {string} country - the current country the user has chosen
    * Function that returns an array of regions of a country
    */
    getRegions = (country) => {
        const { data } = this.state;
        const regions = data[_.findKey(data, item => item.countryName === country)]
            .regions.map(r => r.name);
        return regions;
    }

    /**
     * Handler for child components (FormContainer)
     */
    handleChange = name => value => {
        this.props.form.setFieldsValue({
            [name]: value
        })
    }

    submit = async (e) => {
        e.preventDefault();

        this.setState({ loading: true }); //Start to display the loading icon

        this.props.form.validateFields(
            (err) => {
                if (!err) {
                    //get the token id from OAuth response
                    const { tokenId } = this.props.location.state;
                    const {
                        sex,
                        birthdate,
                        location,
                        region,
                    } = this.props.form.getFieldsValue();

                    //collect the data needed to fetch
                    try {
                        const data = {
                            sex,
                            birth_date: birthdate.format('YYYY-MM-DD'),
                            location: location + ' ' + region,
                            tokenId
                        }
                        fetch(`${domains.host}/api/signup`, {
                            method: 'POST',
                            body: JSON.stringify(data),
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            },
                        })
                            .then(res => res.json())
                            .then(data => {
                                this.setState({ loading: false });
                                console.log(data)
                            })
                    }
                    catch (err) {
                        this.setState({ loading: false });
                        console.log(err)
                    }
                }
                else {
                    this.setState({ loading: false });
                    console.log(err)
                }
            },
        );

    }

    render() {
        const {
            countries,
            loading
        } = this.state;

        const {
            getFieldDecorator,
            getFieldsError,
            getFieldError,
            isFieldTouched,
            getFieldValue
        } = this.props.form;

        // Only show error after a field is touched.
        const locationError = isFieldTouched('location') && getFieldError('location');
        const regionError = isFieldTouched('region') && getFieldError('region');

        return (
            <div>
                <Form onSubmit={this.submit}>
                    <Card
                        className="pswforget-box"
                        data-aos="fade-down"
                        data-aos-duration="1000"
                        data-aos-once="true"
                        bodyStyle={{
                            display: "flex",
                            flexFlow: "row wrap",
                            justifyContent: "center"
                        }}
                    >

                        <div className="middle">
                            <h1>Fill out your information</h1>
                            <FormContainer
                                getFieldDecorator={getFieldDecorator}
                                locationError={locationError}
                                regionError={regionError}
                                handleChange={this.handleChange}
                                countries={countries}
                                getFieldValue={getFieldValue}
                                getRegions={this.getRegions}
                                getFieldsError={getFieldsError}
                                loading={loading}
                            />
                        </div>

                    </Card>
                </Form>
            </div>
        );
    }
}

export default compose(
    Form.create(),
    withRouter
)(Oauth);