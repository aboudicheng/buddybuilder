import React, { useState } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import {
    Card,
    Button,
    Input,
    Icon,
    Menu,
    Dropdown,
} from 'antd';
import styled from 'styled-components';
import axios from 'axios';
import * as domains from '../../constants/api_endpoints';

function Post(props) {
    const [remaining, setRemaining] = useState(props.post.remaining);
    const [join, setJoin] = useState(false);
    const [msgClick, setMsgClick] = useState(false);

    const handleJoin = (id) => async () => {
        const token = localStorage.getItem('token');
        const { email } = props.authUser.data;

        if (!join) {
            setRemaining(prev => prev - 1);
            setJoin(true);
            try {
                const data = await axios.post(`${domains.host}/api/user-event`,
                    {
                        id,
                        operation: 'r',
                        user: email,
                        join: 1
                    },
                    {
                        headers: {
                            'Authorization': 'Token ' + token
                        }
                    })

                console.log(data);
            }
            catch (e) {

            }
        }
        else {
            setRemaining(prev => prev + 1);
            setJoin(false);
            try {
                const data = await axios.post(`${domains.host}/api/user-event`,
                    {
                        id,
                        operation: 'a',
                        user: email,
                        join: 1
                    },
                    {
                        headers: {
                            'Authorization': 'Token ' + token
                        }
                    }
                )

                console.log(data);
            }
            catch (e) {

            }
        }
    }

    const buttonDistance = { margin: " 0 0.8em 0.8em 0 " }

    const JoinNumber = styled.span`
            color: ${join && "#3da80f"};
        `

    const showPostOption = (owner) => {
        const content = [];
        const { authUser } = props;
        if (authUser && (owner === authUser.email)) {
            content.push("Modify", "Delete");
        }
        content.push("Report");

        return content;
    }

    const postContent = (owner) => {
        const options = showPostOption(owner);
        return (
            <Menu>
                {options.map((opt, i) =>
                    <Menu.Item key={i}>{opt}</Menu.Item>
                )}
            </Menu>
        )
    }

    const { post } = props;

    return (
        <>
            <Card
                title={<>{post.name} <Icon type="user" /><JoinNumber>{post.max_size - remaining}</JoinNumber>/{post.max_size}</>}
                extra={
                    <>By {post.owner}
                        <Dropdown overlay={postContent(post.owner)} trigger={['click']}>
                            <Button
                                style={{ marginLeft: "0.8rem" }}
                                icon="ellipsis"
                                size="small"
                                shape="circle"
                            />
                        </Dropdown>
                    </>}
            >
                <p>{post.description}</p>
                <div className="user-actions">
                    <div className="actions">
                        <Button
                            style={buttonDistance}
                            icon="user-add"
                            size="large"
                            onClick={handleJoin(post.id)}
                            type={join ? "primary" : "default"}
                        >Join</Button>
                        <Button style={buttonDistance} icon="heart" size="large">Interested</Button>
                        <Button style={buttonDistance} icon="message" size="large" onClick={() => { setMsgClick(prevState => !prevState) }}>Comment</Button>
                        <Button style={buttonDistance} icon="share-alt" size="large">Share</Button>
                    </div>

                </div>
            </Card>

            {msgClick &&
                <Card style={{
                    position: "relative",
                    bottom: 20,
                    marginBottom: 0
                }}>
                    <Input placeholder="Comment here..." />
                </Card>
            }
        </>
    );
}

const mapStateToProps = (state) => ({
    authUser: state.sessionState.authUser
})

export default compose(
    connect(mapStateToProps)
)(Post);