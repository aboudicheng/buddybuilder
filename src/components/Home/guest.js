import React, { useEffect } from 'react';
import { compose } from 'recompose';
import { withRouter } from "react-router";
import { Button } from 'antd';
import styled from 'styled-components';
import * as routes from '../../constants/routes';

const GuestPage = styled.div`
display: flex;
align-items: center;
justify-content: center;
height: calc(100vh - 64px);
`

const HomeText = styled.div`
text-align: center;
font-size: 15pt;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`

const ButtonWrapper = styled.div`
margin-bottom: 0.8em;
`

function Guest(props) {
    useEffect(() => {
        document.title = 'BuddyBuilder - Find your best sport partners';
    })

    function redirect(route) {
        props.history.push(route);
    }

    return (
        <GuestPage>
            <HomeText>
                <h1 data-aos="fade-down" data-aos-duration="1000" data-aos-once="true">Find your best sport partners</h1>
                <h3 data-aos="fade-up" data-aos-once="true">Nothing more than just a quick search.</h3>
                <ButtonWrapper>
                    <Button
                        style={{ backgroundColor: "#16a085", color: "#DDD" }}
                        onClick={() => redirect(routes.LOGIN)}
                        size="large"
                        data-aos="zoom-in"
                        data-aos-duration="1000"
                        data-aos-once="true"
                    >
                        Get Started
                    </Button>
                </ButtonWrapper>
            </HomeText>
        </GuestPage>
    )
}

export default compose(
    withRouter
)(Guest);