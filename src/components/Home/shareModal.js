import React from 'react';
import { Modal } from 'antd';
import { ShareButtonRoundSquare, ShareBlockStandard } from "react-custom-share";
import { FaTwitter, FaFacebook, FaGooglePlus } from "react-icons/fa";

const shareBlockProps = {
    url: "https://github.com/greglobinski/react-custom-share",
    button: ShareButtonRoundSquare,
    buttons: [
        { network: "Twitter", icon: FaTwitter },
        { network: "Facebook", icon: FaFacebook },
        { network: "GooglePlus", icon: FaGooglePlus },
    ],
};

const ShareModal = (props) => {
    return (
        <Modal
            title="Select the platform"
            visible={props.visible}
            onOk={props.handleModal(false)}
            onCancel={props.handleModal(false)}
        >
            <ShareBlockStandard {...shareBlockProps} />
        </Modal>
    )
}

export default ShareModal;