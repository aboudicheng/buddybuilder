import React, { useState, useEffect } from 'react';
import {
    Drawer,
    Form,
    Button,
    Col,
    Row,
    Input,
    Select,
    DatePicker,
    InputNumber,
    TimePicker,
    Checkbox
} from 'antd';
import styled from 'styled-components';
import moment from 'moment';
import _ from 'lodash';
import axios from 'axios';

const format = 'HH:mm';

const { Option } = Select;

const SubmitButtons = styled.div`
position: absolute;
bottom: 0;
width: 100%;
border-top: 1px solid #e8e8e8;
padding: 10px 16px;
text-align: right;
left: 0;
background: #fff;
border-radius: 0 0 4px 4px;
`

function EventDrawer(props) {
    const [sportTypes, setSportTypes] = useState([]);
    const [loading, setLoading] = useState(false);
    const [data, setData] = useState(false);
    const [countries, setCountries] = useState([]);

    useEffect(() => {
        (async () => {
            try {
                const sportResp = await axios.get(`${process.env.PUBLIC_URL}/sports.json`)
                const sportTypes = await sportResp.data.sort();
                sportTypes.push("Other"); //Push an "other" option at the end of the list

                //get countries & regions data
                const countryResp = await axios.get(`${process.env.PUBLIC_URL}/data.json`);

                let countries = [];
                countryResp.data.forEach(n => {
                    countries.push(n.countryName);
                })

                setData(countryResp.data);
                setCountries(countries);
                setSportTypes(sportTypes);
            }
            catch (e) {
                console.log(e);
            }
        })();
    }, []);

    /**
    * @param {string} country - the current country the user has chosen
    * Function that returns an array of regions of a country
    */
    function getRegions(country) {
        const regions = data[_.findKey(data, item => item.countryName === country)]
            .regions.map(r => r.name);
        return regions;
    }

    const handleChange = name => value => {
        props.form.setFieldsValue({
            [name]: value
        })
    }

    function submit(e) {
        e.preventDefault();

        setLoading(true); //Start to display the loading icon

        props.form.validateFields(
            (err) => {
                setLoading(false);
                if (!err) {
                    props.handleDrawer(false)();

                    //TODO: re-initialize form inputs
                    props.form.setFieldsValue({
                        sportType: null,
                        dateTime: null,
                        location: null,
                        description: null
                    })
                }
                else {
                    console.log(err);
                }
            }
        );
    }

    const {
        getFieldDecorator,
        getFieldsValue,
        setFieldsValue
    } = props.form;
    const { singleDay, country } = getFieldsValue();

    return (
        <>
            <Drawer
                title="Organize your own sport event!"
                width={window.innerWidth > 720 ? 720 : "100%"}
                placement="right"
                onClose={props.handleDrawer(false)}
                maskClosable={true}
                visible={props.visible}
                style={{
                    height: 'calc(100% - 55px)',
                    overflow: 'auto',
                    paddingBottom: 53,
                }}
            >
                <Form layout="vertical" hideRequiredMark onSubmit={submit}>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Form.Item label="Sport Type">
                                {getFieldDecorator('sportType', {
                                    rules: [{ required: true, message: 'Please select a sport type' }],
                                })(
                                    <Select
                                        mode="multiple"
                                        placeholder="Choose a sport type"
                                        required
                                    >
                                        {sportTypes.map((sport, i) =>
                                            <Option value={sport} key={`${sport}-${i}`}>{sport}</Option>
                                        )}
                                    </Select>
                                )}
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item label="Date">
                                {getFieldDecorator('dateTime', {
                                    rules: [{ required: true, message: 'Please choose the date' }],
                                })(
                                    singleDay
                                        ? <DatePicker getPopupContainer={trigger => trigger.parentNode} />
                                        : <DatePicker.RangePicker
                                            style={{ width: '100%' }}
                                            getPopupContainer={trigger => trigger.parentNode}
                                        />
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('singleDay', {
                                    valuePropName: 'checked',
                                    initialValue: true,
                                })(
                                    <Checkbox>Single Day</Checkbox>
                                )}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Form.Item label="Begin Time">
                                {getFieldDecorator('beginTime', {
                                    rules: [{ required: true, message: 'Please choose a time' }],
                                    initialValue: moment('00:00', format)
                                })(
                                    <TimePicker format={format} />
                                )}
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item label="End Time">
                                {getFieldDecorator('endTime', {
                                    rules: [{ required: true, message: 'Please choose a time' }],
                                    initialValue: moment('00:00', format)
                                })(
                                    <TimePicker format={format} />
                                )}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Form.Item label="Country">
                                {getFieldDecorator('country', {
                                    rules: [{ required: true, message: 'Please input your country!' }],
                                    initialValue: ''
                                })(
                                    <Select
                                        onChange={value => { handleChange('country'); setFieldsValue({ region: getRegions(value)[0] }) }}
                                    >
                                        {countries.length !== 0 && countries.map(country => <Select.Option key={country}>{country}</Select.Option>)}
                                    </Select>
                                )}
                            </Form.Item>

                        </Col>
                        <Col span={12}>
                            <Form.Item label="Region:">
                                {getFieldDecorator('region', {
                                    rules: [{ required: true, message: 'Please input your region!' }],
                                    initialValue: ''
                                })(
                                    <Select
                                        onChange={handleChange('region')}
                                    >
                                        {!!country && getRegions(country).map(city => <Select.Option key={city}>{city}</Select.Option>)}
                                    </Select>
                                )}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={12}>
                            <Form.Item label="Location">
                                {getFieldDecorator('location', {
                                    rules: [{ required: true, message: 'Please choose a location' }],
                                })(
                                    <Input
                                        placeholder="Landmark or address"
                                        style={{ width: "100%" }}
                                    />
                                )}
                            </Form.Item>
                        </Col>
                        <Col span={12}>
                            <Form.Item label="Participation Number">
                                {getFieldDecorator('participationNo', {
                                    rules: [{ required: true, message: 'Please provide number of participants' }],
                                    initialValue: 2
                                })(
                                    <InputNumber
                                        style={{ width: '100%' }}
                                        min={1}
                                        max={50}
                                        parser={value => value.replace('.', '')}
                                        placeholder="Choose the participation number"
                                    />
                                )}
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item label="Description">
                                {getFieldDecorator('description', {
                                    rules: [
                                        {
                                            required: true,
                                            message: 'Please provide a description for this event!',
                                        },
                                    ],
                                })(<Input.TextArea rows={4} placeholder="Please provide a description for this event" required />)}
                            </Form.Item>
                        </Col>
                    </Row>
                    <SubmitButtons>
                        <Button
                            style={{
                                marginRight: 8,
                            }}
                            onClick={props.handleDrawer(false)}
                        >
                            Cancel
                        </Button>

                        <Button
                            type="primary"
                            htmlType="submit"
                            loading={loading}
                        >Submit
                        </Button>
                    </SubmitButtons>
                </Form>
            </Drawer>
        </>
    );

}

export default Form.create()(EventDrawer);