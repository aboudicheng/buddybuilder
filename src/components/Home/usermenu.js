import React, { Component } from 'react';
import { Card, Button, Tooltip, Icon } from 'antd';
import InfiniteScroll from 'react-infinite-scroller';
import EventDrawer from './EventDrawer';
import './usermenu.scss';
import Post from './post';
import * as domains from '../../constants/api_endpoints';

class UserMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            drawer: false,
            posts: [],
            hasMore: true,
        }

    }

    componentDidMount() {
        document.title = 'Home';
    }

    fetchEvents = async () => {
        // fetch('https://reqres.in/api/users')
        //     .then(res => res.json())
        //     .then(json => {
        //         const items = [...this.state.posts];

        //         for (let i = 0; i < 3; i++)
        //             items.push(<Post key={i} index={i} />);

        //         this.setState({
        //             posts: items,
        //         })
        //     })
        const token = localStorage.getItem('token');
        fetch(`${domains.host}/api/event`, {
            method: 'GET',
            headers: new Headers({
                'Authorization': 'Token ' + token
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            const { results } = data;
            const items = [...this.state.posts];

            results.forEach((post, i) => {
                items.push(<Post key={i} index={i} post={post} />)
            })

            this.setState({ posts: items });
        })
        .catch(e => {
            console.log(e)
        })
    }

    handleDrawer = (visible) => (e) => {
        this.setState({ drawer: visible });
    }

    render() {
        const { drawer } = this.state;

        return (
            <div className="user-page">
                <EventDrawer visible={drawer} handleDrawer={this.handleDrawer} />
                <div className="posts">
                    <InfiniteScroll
                        pageStart={0}
                        loadMore={this.fetchEvents}
                        hasMore={this.state.hasMore}
                        loader={<div style={{ textAlign: "center" }}><Icon type="loading" key={0} style={{ fontSize: 36 }} /></div>}
                    >
                        {this.state.posts}
                    </InfiniteScroll>
                </div>
                <div className="right-col">
                    <Card
                        title="Home"
                    >
                        <p>Organize a sport event and invite your friends to join in!</p>
                        <Button
                            type="primary"
                            icon="edit"
                            size="large"
                            onClick={this.handleDrawer(true)}
                        >Create event</Button>
                    </Card>
                </div>
                <Tooltip placement="topRight" title="Create event">
                    <Button
                        type="primary"
                        shape="circle"
                        icon="plus"
                        size="large"
                        className="create-event-btn"
                        onClick={this.handleDrawer(true)}
                    />
                </Tooltip>
            </div>
        );
    }
}

export default UserMenu;