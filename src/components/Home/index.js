import React from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import UserMenu from './usermenu';
import Guest from './guest';

/**
 * Component returns different UI components
 * according to user's login status
 */
const Home = (props) =>
    <>
        {props.authUser ? <UserMenu /> : <Guest />}
    </>

const mapStateToProps = (state) => ({
    authUser: state.sessionState.authUser
})

export default compose(
    connect(mapStateToProps)
)(Home);