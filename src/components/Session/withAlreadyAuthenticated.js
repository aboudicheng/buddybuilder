import { useEffect } from 'react';
import * as routes from '../../constants/routes';

function useWithAlreadyAuthenticated(props) {
    useEffect(() => {
        if (props.authUser) {
            props.history.push(routes.HOME);
        }
    })
}

export default useWithAlreadyAuthenticated;