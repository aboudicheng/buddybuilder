import React from "react";
import { connect } from "react-redux";
import { compose } from "recompose";
import { withRouter } from "react-router-dom";

import * as routes from "../../constants/routes";

const withAuthorization = (condition) => (Component) => {
    class WithAuthorization extends React.Component {
        componentDidMount() {
            /**
            * Fetch localStorage's user token to server if it exists
            * else set authUser to null
            */
            const user = localStorage.getItem('user');
            if (!!user) {
                //TODO: Fetch token to server for verification

                //if error occures push to Login page
                if (!condition(user)) {
                    this.props.history.push(routes.LOGIN);
                }
            }
            else {
                //push to Login page directly
                this.props.history.push(routes.LOGIN);
            }
        }

        render() {
            return this.props.authUser ? <Component /> : null;
        }
    }

    const mapStateToProps = (state) => ({
        authUser: state.sessionState.authUser,
    });

    return compose(
        withRouter,
        connect(mapStateToProps),
    )(WithAuthorization);
};

export default withAuthorization;