import { useEffect } from "react";
import axios from 'axios';
import * as domains from '../../constants/api_endpoints';

async function authenticate(props) {
    const { onSetAuthUser } = props;

    /**
     * Fetch localStorage's user token to server if it exists
     * else set authUser to null
     */
    const token = localStorage.getItem('token');
    if (!!token) {
        try {
            const { data } = await axios.get(`${domains.host}/api/user`, {
                method: 'GET',
                headers: {
                    'Authorization': 'Token ' + token
                }
            })

            if (!!data.detail) {
                onSetAuthUser(null);
                localStorage.removeItem('token');
            }
            else {
                onSetAuthUser(data);
            }
        }
        catch (e) {
            onSetAuthUser(null);
            console.log(e)
        }
    }
    else {
        onSetAuthUser(null);
    }
}

function useWithAuthentication(props) {
    useEffect(() => {
        authenticate(props)
    })
}

export default useWithAuthentication;