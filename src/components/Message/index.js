import React, { Component } from 'react';
import styled from 'styled-components';
import {
    Avatar,
    Icon
} from 'antd';
import './message.scss';

const Container = styled.div`
display: flex;
min-width: 300px;
height: calc(100vh - 64px);
color: rgba(189, 189, 192, 1);
`;

const UserFrame = styled.div`
display: flex;
align-items: flex-start;
padding: 1rem;
justify-content: center;
:hover {
    background: rgba(0, 0, 0, 0.2);
    cursor: pointer;
}
:focus {
    background: rgba(0, 0, 0, 0.2);
}
`

const Main = styled.main`
flex: 1 1;
background-color: rgba(25, 25, 27, 1);
height: 100%;
width: 50%;
`

const ChatWindow = styled.div`
display: flex;
flex-direction: column;
height: calc(100vh - 64px);
`

const Header = styled.header`
padding: 1rem 2rem;
border-bottom: 1px solid rgba(189, 189, 192, 0.2);
`

const Chats = styled.div`
flex: 1 1;
display: flex;
flex-direction: column;
align-items: flex-start;
width: 85%;
margin: 0 auto;
word-wrap: break-word;
`

const Send = styled.form`
width: 80%;
margin: 1rem auto;
`

const User = () =>
    <UserFrame>
        <div style={{ height: 50, width: 50 }}><Avatar icon="user" size={50} /></div>
        <div className="user_details">
            <p className="username">Name Surname</p>
            <p>Lorem ipsum dolor sit amet</p>
        </div>
    </UserFrame>

class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            isLoading: true,
            inputValue: ""
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        fetch('https://reqres.in/api/users')
            .then(res => res.json())
            .then(() => {
                const messages = [{ message: "Hello", user: true }, { message: "wzup", user: false }, { message: "stfu", user: true }, { message: "ok", user: false }];
                this.setState({ messages, isLoading: false }, () => {
                    this.scrollToBottom();
                });
            })
            .catch(e => {
                console.log(e)
            })
    }

    handleChange = name => e => {
        this.setState({
            [name]: e.target.value
        })
    }

    scrollToBottom = () => {
        this.messagesEnd.scrollTop = this.messagesEnd.scrollHeight;
    }

    sendMessage = e => {
        e.preventDefault();

        this.setState(prevState => ({
            messages: [...prevState.messages, { message: this.state.inputValue, user: true }],
            inputValue: ""
        }), () => {
            this.scrollToBottom();
        })
    }

    render() {
        const { messages, isLoading } = this.state;
        return (
            <Container>
                <div className="sidebar scrollbar">
                    <User />
                    <User />
                    <User />
                    <User />
                    <User />
                    <User />
                    <User />
                    <User />
                    <User />
                    <User />
                </div>
                <Main>
                    <ChatWindow>
                        <Header>
                            <h1 style={{ color: "#fff" }}>Name Surname</h1>
                            <p>Lorem ipsum dolor sit amet</p>
                        </Header>
                        <Chats ref={el => { this.messagesEnd = el }} className="scrollbar">
                            <>
                                {isLoading
                                    ? <div style={{ margin: "1em auto" }}><Icon type="loading" key={0} style={{ fontSize: 36 }} /></div>
                                    : messages.map((msg, i) =>
                                        <div className={`chat ${msg.user && "user"}`} key={i}>{msg.message}</div>
                                    )}
                            </>
                        </Chats>
                        <Send onSubmit={this.sendMessage}>
                            <input className="message_input" placeholder="Write your message" value={this.state.inputValue} onChange={this.handleChange("inputValue")} />
                        </Send>
                    </ChatWindow>
                </Main>
            </Container>
        );
    }
}

export default Message;