import React, { useState, useEffect } from 'react';
import {
    Input,
    Button,
    Icon,
    Card,
    Form,
    Tooltip,
    message
} from 'antd';
import axios from 'axios';
import Message from '../../containers/message';
import * as domains from '../../constants/api_endpoints';
import * as validators from '../../helpers/formValidation';
import './PasswordForget.scss';

const messageTitle = "Email Sent";
const messageContent = "An email has been sent to your email address. Please follow the instructions to reset your password.";

const FormItem = Form.Item;

function PasswordForget(props) {
    const [success, setSuccess] = useState(false);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        document.title = 'Forgot Password';
    })

    function submit(e) {
        e.preventDefault();

        setLoading(true); //Start to display the loading icon

        props.form.validateFields(
            async (err) => {
                if (!err) {
                    const { email } = props.form.getFieldsValue();
                    try {
                        const requestBody = {
                            email
                        }
                        const { data } = await axios.post(`${domains.host}/api/forgot`, requestBody, {
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            }
                        })

                        if (!!data.error) {
                            message.error(data.error);
                            setLoading(false);
                        }
                        else {
                            setLoading(false);
                            setSuccess(true);
                        }
                    }
                    catch (error) {
                        setLoading(false);
                        console.log(error)
                    }
                }
                else {
                    setLoading(false);
                    console.log(err);
                }
            }
        )
    }

    const {
        getFieldDecorator,
        getFieldError,
    } = props.form;

    const emailError = getFieldError('email');
    const flexStyle = {
        display: "flex",
        flexFlow: "row wrap",
        justifyContent: "center"
    }

    return (
        <div>
            {success
                ? <Message message={messageContent} title={messageTitle} />
                : <Form onSubmit={submit}>
                    <Card
                        className="pswforget-box"
                        data-aos="fade-down"
                        data-aos-duration="1000"
                        data-aos-once="true"
                        bodyStyle={flexStyle}
                    >
                        <div className="middle">
                            <h1>Forgot Password?</h1>
                            <p>Enter your email below and we will email you with instructions on how to reset your password.</p>

                            <FormItem
                                validateStatus={emailError ? 'error' : ''}
                                help={''}
                                required
                            >
                                {getFieldDecorator('email', {
                                    validateTrigger: 'onBlur',
                                    rules: [
                                        { validator: validators.validateEmail }
                                    ],
                                    initialValue: ''
                                })(
                                    <Tooltip placement="top" title={emailError} trigger="focus">
                                        <Input
                                            size="large"
                                            type="email"
                                            placeholder="Email"
                                            prefix={<Icon type="mail" />}
                                            required
                                        />
                                    </Tooltip>
                                )}
                            </FormItem>

                            <Button
                                style={{ backgroundColor: "#16a085", color: "#DDD" }}
                                size="large"
                                htmlType="submit"
                                loading={loading}
                            >Send</Button>
                        </div>

                    </Card>
                </Form>
            }
        </div>
    );
}

export default Form.create()(PasswordForget);