import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import Navigation from '../Navigation';
import Home from '../Home';
import Profile from '../Profile';
import Message from '../Message';
import SignUp from '../Signup';
import Login from '../Login';
import Oauth from '../Oauth';
import PasswordForget from '../PasswordForget';
import Reset from '../Reset';
import NotFound from '../NotFound';
import Footer from '../Footer';
import * as routes from '../../constants/routes';
import * as actions from '../../actions';
import useWithAuthentication from "../Session/withAuthentication";
import { Layout, Icon } from 'antd';
import styled from 'styled-components';
import './App.scss';

const Loader = styled.div`
height: calc(100vh - 64px);
display: flex;
align-items: center;
justify-content: center;
`

function App(props) {
  //Authenticate user if user has token
  useWithAuthentication(props);

  if (props.loading)
    return <Loader><Icon type="loading" style={{ fontSize: 48 }} /></Loader>

  return (
    <Router basename={process.env.PUBLIC_URL}>
      <Layout className="App">
        <Navigation />
        <Switch>
          <Route exact path={routes.HOME} component={() => <Home />} />
          <Route exact path={routes.SIGNUP} component={() => <SignUp />} />
          <Route exact path={routes.LOGIN} component={() => <Login />} />
          <Route exact path={routes.PROFILE} component={() => <Profile />} />
          <Route exact path={routes.MESSAGE} component={() => <Message />} />
          <Route exact path={routes.PASSWORD_FORGET} component={() => <PasswordForget />} />
          <Route exact path={routes.OAUTH} component={() => <Oauth />} />
          <Route exact path={routes.RESET} component={props => <Reset {...props} />} />
          <Route component={NotFound} />
        </Switch>
        <Footer />
      </Layout>
    </Router>
  );
}

const mapStateToProps = (state) => ({
  loading: state.sessionState.loading
})

const mapDispatchToProps = (dispatch) => ({
  onSetAuthUser: (authUser) => dispatch(actions.onSetAuthUser(authUser)),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps)
)(App);
