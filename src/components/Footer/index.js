import React from 'react';
import { List, Card, Layout } from 'antd';

const data = [
    {
        title: 'Title 1',
    },
    {
        title: 'Title 2',
    },
    {
        title: 'Title 3',
    },
    {
        title: 'Title 4',
    },
];

const Footer = () => (
    <Layout.Footer>
        <List
            grid={{ gutter: 16, column: 4 }}
            dataSource={data}
            renderItem={item => (
                <List.Item>
                    <Card title={item.title}>Card content</Card>
                </List.Item>
            )}
        />
    </Layout.Footer>
)

export default Footer;