import React from 'react';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import UserNav from './usernav';
import GuestNav from './guestnav';

/**
 * Component returns different UI components
 * according to user's login status
 */
const Navigation = (props) =>
    <>
        {props.authUser ? <UserNav /> : <GuestNav />}
    </>

const mapStateToProps = (state) => ({
    authUser: state.sessionState.authUser
})

export default compose(
    connect(mapStateToProps)
)(Navigation);