import React, { Component } from 'react';
import { connect } from "react-redux";
import * as actions from '../../actions';
import { compose } from 'recompose';
import { withRouter } from "react-router";
import {
    Layout,
    Menu,
    Input,
    Dropdown,
    Icon,
    Avatar,
    Drawer
} from 'antd';
import * as routes from '../../constants/routes';
import './usernav.scss';

const Search = Input.Search;
const { Header } = Layout;
const { Item, Divider } = Menu;

const handleClick = (props) => (e) => {
    switch (e.key) {
        case "1":
            props.history.push(routes.PROFILE);
            break;
        case "3":
            props.history.push(routes.MESSAGE);
            break;
        case "4":
            props.history.push(routes.SETTINGS);
            break;
        case "5":
            localStorage.removeItem('token');
            props.onSetAuthUser(null);

            props.history.push(routes.LOGIN);
            break;
        default: return;
    }
}

const menu = (props) => (
    <Menu theme="dark" onClick={handleClick(props)}>
        <Item key="1">
            <Icon type="user" /> Profile
        </Item>
        <Divider />
        <Item key="2">
            <Icon type="notification" /> Notification
        </Item>
        <Divider />
        <Item key="3">
            <Icon type="message" /> Message
        </Item>
        <Divider />
        <Item key="4">
            <Icon type="setting" /> Settings
        </Item>
        <Divider />
        <Item key="5">
            <Icon type="logout" /> Log out
        </Item>
    </Menu>
)

class UserNav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchVisible: false,
            menuVisible: false,
        }
    }

    handleDrawer = (icon, visible) => (e) => {
        this.setState({ [icon]: visible })
    }

    render() {
        const avatarStyle = {
            cursor: "pointer",
            margin: "0 0.7em",
            backgroundColor: 'rgb(22, 160, 133)'
        }
        return (
            <Layout>
                <Header className="user-header">

                    {/* Mobile */}
                    <div className="header-menu-icon">
                        <Avatar icon="bars" style={avatarStyle} onClick={this.handleDrawer("menuVisible", true)} />
                        <Drawer
                            placement="left"
                            closable={false}
                            onClose={this.handleDrawer("menuVisible", false)}
                            visible={this.state.menuVisible}
                        >
                            <Menu onClick={(e) => { handleClick(this.props)(e); this.handleDrawer("menuVisible", false)(e) }}>
                                <Item key="1">
                                    <Icon type="user" /> Profile
                                </Item>
                                <Item key="2">
                                    <Icon type="notification" /> Notification
                                </Item>
                                <Item key="3">
                                    <Icon type="message" /> Message
                                </Item>
                                <Item key="4">
                                    <Icon type="setting" /> Settings
                                </Item>
                                <Item key="5">
                                    <Icon type="logout" /> Log out
                                </Item>
                            </Menu>
                        </Drawer>
                    </div>

                    <div className="logo" onClick={() => this.props.history.push(routes.HOME)}>BuddyBuilder</div>

                    {/* Desktop */}
                    <div className="header-search-icon">
                        <Avatar icon="search" style={avatarStyle} onClick={this.handleDrawer("searchVisible", true)} />
                        <Drawer
                            placement="top"
                            closable={true}
                            onClose={this.handleDrawer("searchVisible", false)}
                            visible={this.state.searchVisible}
                            height="100px"
                            style={{ display: "flex", justifyContent: "center", alignItems: "center" }}
                        >
                            <Search
                                placeholder="Search"
                                onSearch={value => console.log(value)}
                                style={{ width: 300, height: 40 }}
                            />
                        </Drawer>
                    </div>

                    <div className="header-search">
                        <Search
                            placeholder="Search"
                            onSearch={value => console.log(value)}
                            style={{ width: 400, height: 40 }}
                        />
                    </div>
                    <Dropdown
                        overlay={menu(this.props)}
                        trigger={['click']}
                        className="avatar-dropdown"
                        placement="bottomRight"
                    >
                        <div>
                            <Avatar icon="user" style={avatarStyle} />
                            <Icon type="down" style={{ color: "#16a085" }} />
                        </div>
                    </Dropdown>
                </Header>
            </Layout>
        );
    }
}


const mapDispatchToProps = (dispatch) => ({
    onSetAuthUser: (authUser) => dispatch(actions.onSetAuthUser(authUser)),
});

export default compose(
    withRouter,
    connect(null, mapDispatchToProps)
)(UserNav);