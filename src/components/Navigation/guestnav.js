import React from 'react';
import { compose } from 'recompose';
import { Link } from 'react-router-dom';
import { withRouter } from "react-router";
import { Layout, Menu } from 'antd';
import * as routes from '../../constants/routes';

const headerStyle = {
    display: 'flex',
    justifyContent: 'space-between'
}

const { Header } = Layout;

const GuestNav = (props) =>
    <Layout>
        <Header style={headerStyle}>
            <div className="logo" onClick={() => props.history.push(routes.HOME)}>BuddyBuilder</div>
            <Menu
                theme="dark"
                mode="horizontal"
                style={{ lineHeight: '64px' }}
            >
                <Menu.Item key="1"><Link to={routes.SIGNUP}>Sign up</Link></Menu.Item>
                <Menu.Item key="2"><Link to={routes.LOGIN}>Login</Link></Menu.Item>
            </Menu>
        </Header>
    </Layout>

export default compose(
    withRouter
)(GuestNav);