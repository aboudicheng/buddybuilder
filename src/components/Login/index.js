import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import {
    Input,
    Button,
    Row,
    Icon,
    Card,
    Form,
    message,
    Tooltip
} from 'antd';
import { withRouter } from "react-router";
import { Link } from 'react-router-dom';
import useWithAlreadyAuthenticated from '../Session/withAlreadyAuthenticated';
import axios from 'axios';
import * as domains from '../../constants/api_endpoints';
import * as routes from '../../constants/routes';
import * as validators from '../../helpers/formValidation';
import * as actions from '../../actions';

const FormItem = Form.Item;

function Login(props) {
    const [success, setSuccess] = useState(false);
    const [loading, setLoading] = useState(false);

    useWithAlreadyAuthenticated(props);

    useEffect(() => {
        /**
         * Retrieve query string from the URL if the user
         * comes via clicking email verification link in order to
         * display a success message
         */
        document.title = 'Login';
        const params = new URLSearchParams(props.location.search);
        const success = params.get('active');

        if (success === 'success') {
            setSuccess(true);
        }
    })

    function submit(e) {
        e.preventDefault();

        setLoading(true); //Start to display the loading icon

        props.form.validateFields(
            async (err) => {
                if (!err) {
                    const { email, password } = props.form.getFieldsValue();

                    //collect the data needed to fetch
                    try {
                        const requestBody = {
                            email,
                            password,
                        }
                        const { data } = await axios.post(`${domains.host}/api/login`, requestBody, {
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json'
                            }
                        })

                        if (!!data.error) {
                            message.error(data.error);
                            setLoading(false);
                        }
                        else {
                            const { token, user } = data;
                            localStorage.setItem('token', token);
                            props.onSetAuthUser(user);
                            props.history.push(routes.HOME);
                            setLoading(false);
                        }
                    }
                    catch (error) {
                        setLoading(false);
                        message.error(error)
                    }
                }
                else {
                    setLoading(false);
                    console.log(err)
                }
            },
        );
    }

    const {
        getFieldDecorator,
        getFieldError,
    } = props.form;

    const emailError = getFieldError('email');
    const passwordError = getFieldError('password');

    return (
        <div>
            <Form onSubmit={submit}>
                <Card
                    className="login-box"
                    data-aos="fade-down"
                    data-aos-duration="1000"
                    data-aos-once="true"
                    bodyStyle={{
                        display: "flex",
                        flexFlow: "row wrap",
                        justifyContent: "center"
                    }}
                >
                    <div className="left">
                        <h1>Login</h1>
                        {success && <h4 style={{ color: "green" }}>Activation Successful!</h4>}

                        <FormItem
                            validateStatus={emailError ? 'error' : ''}
                            help={''}
                        >
                            {getFieldDecorator('email', {
                                validateTrigger: 'onBlur',
                                rules: [
                                    { validator: validators.validateEmail }
                                ],
                                initialValue: ''
                            })(
                                <Tooltip placement="top" title={emailError} trigger="focus">
                                    <Input
                                        id="login-email"
                                        size="large"
                                        type="email"
                                        placeholder="Email"
                                        prefix={<Icon type="mail" />}
                                        required
                                    />
                                </Tooltip>
                            )}
                        </FormItem>

                        <FormItem
                            validateStatus={passwordError ? 'error' : ''}
                            help={''}
                        >
                            {getFieldDecorator('password', {
                                validateTrigger: 'onBlur',
                                rules: [
                                    { validator: validators.validateToNextPassword }
                                ],
                                initialValue: ''
                            })(
                                <Tooltip placement="top" title={passwordError} trigger="focus">
                                    <Input
                                        id="login-password"
                                        size="large"
                                        type="password"
                                        placeholder="Password"
                                        prefix={<Icon type="lock" />}
                                        required
                                    />
                                </Tooltip>
                            )}
                        </FormItem>

                        <Row>
                            <Link to={routes.PASSWORD_FORGET}>Forgot Password</Link>
                        </Row>
                        <br />

                        <Button style={{ backgroundColor: "#16a085", color: "#DDD" }} size="large" loading={loading} htmlType="submit">Sign in</Button>
                    </div>

                    <div className="right">
                        <h1 className="loginwith">Sign in/up with<br />social network</h1>

                        <button className="social-signin facebook" data-aos="fade-up" data-aos-delay="500" data-aos-duration="1000" data-aos-once="true">Log in with Facebook</button>

                        <button className="social-signin google" data-aos="fade-up" data-aos-delay="500" data-aos-duration="1000" data-aos-once="true">Log in with Google</button>

                        <button className="social-signin twitter" data-aos="fade-up" data-aos-delay="500" data-aos-duration="1000" data-aos-once="true">Log in with Twitter</button>
                    </div>
                </Card>
            </Form>
        </div>
    );
}

const mapStateToProps = (state) => ({
    authUser: state.sessionState.authUser
})

const mapDispatchToProps = (dispatch) => ({
    onSetAuthUser: (authUser) => dispatch(actions.onSetAuthUser(authUser))
})

export default compose(
    withRouter,
    Form.create(),
    connect(mapStateToProps, mapDispatchToProps)
)(Login);