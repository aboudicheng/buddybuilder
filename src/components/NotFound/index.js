import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import * as routers from '../../constants/routes';

const NotFound = () =>
    <div className="guest-page">
        <div className="guest">
            <h1>Oh No! Page not found!</h1>
            <h3>Go back to <Link to={routers.HOME}>homepage</Link></h3>
        </div>
    </div>

export default withRouter(NotFound);