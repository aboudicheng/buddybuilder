import React from 'react';
import { Provider } from "react-redux";
import store from "../../store";
import App from '../App';

describe('App', () => {
  it('should render correctly in "debug" mode', () => {
    const component = shallow(
      <Provider store={store}>
        <App debug />
      </Provider>
    );

    expect(component).toMatchSnapshot();
  });
});
