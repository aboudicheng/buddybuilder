import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import Guest from '../Home/guest';

describe('Guest', () => {
    it('should render correctly in "debug" mode', () => {
        const component = mount(
            <MemoryRouter keyLength={0}>
                <Guest debug />
            </MemoryRouter>
        );

        expect(component).toMatchSnapshot();
    });
})
