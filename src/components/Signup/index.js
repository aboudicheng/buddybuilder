import React, { useState, useEffect } from 'react';
import {
    Input,
    Icon,
    Card,
    Form,
    message,
    Tooltip
} from 'antd';
import _ from 'lodash';
import FormContainer from '../../containers/form';
import Message from '../../containers/message';
import * as domains from '../../constants/api_endpoints';
import * as validators from '../../helpers/formValidation';
import * as stringHelpers from '../../helpers/stringHelper';
import axios from 'axios';
import './signup.scss';

const FormItem = Form.Item;

/**
 * Messages to display when the user successfully presses
 * the signup button
 */
const messageTitle = "Email Verification";
const messageContent = "Thank you for registering! Please check your inbox and verify your account."

function SignUp(props) {
    const [data, setData] = useState(null);
    const [countries, setCountries] = useState([]);
    const [success, setSuccess] = useState(false);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        document.title = 'Sign up';
        (async function () {
            try {
                //get countries & regions data
                const response = await axios.get(`${process.env.PUBLIC_URL}/data.json`);
                let countries = [];
                response.data.forEach(n => {
                    countries.push(n.countryName);
                })
                setData(response.data);
                setCountries(countries);
            }
            catch (e) {
                console.log(e);
            }
        })();
    }, [])

    /**
    * @param {string} country - the current country the user has chosen
    * Function that returns an array of regions of a country
    */
    function getRegions(country) {
        const regions = data[_.findKey(data, item => item.countryName === country)]
            .regions.map(r => r.name);
        return regions;
    }

    /**
    * Handler for child components (FormContainer)
    */
    const handleChange = name => value => {
        props.form.setFieldsValue({
            [name]: value
        })
    }

    function submit(e) {
        e.preventDefault();

        setLoading(false); //Start to display the loading icon

        props.form.validateFields(
            async (err) => {
                if (!err) {
                    const {
                        firstName,
                        lastName,
                        email,
                        password,
                        confirm,
                        sex,
                        birthdate,
                        location,
                        region,
                    } = props.form.getFieldsValue();

                    //collect the data needed to fetch
                    try {
                        const requestBody = {
                            first_name: stringHelpers.capitalizeFirstLetter(firstName),
                            last_name: stringHelpers.capitalizeFirstLetter(lastName),
                            email,
                            password,
                            confirm,
                            sex,
                            birth_date: birthdate.format('YYYY-MM-DD'),
                            country: location,
                            city: region,
                            username: ""
                        }
                        const result = await axios.post(`${domains.host}/api/signup`, requestBody);

                        if (!!result.data.error) {
                            message.error(result.data.error)
                            setLoading(false);
                        }
                        else {
                            setLoading(false);
                            setSuccess(true);
                        }
                    }
                    catch (error) {
                        setLoading(false);
                        message.error(error);
                    }
                }
                else {
                    setLoading(false);
                }
            }
        );
    }

    const {
        getFieldDecorator,
        getFieldsError,
        getFieldError,
        getFieldValue,
        setFieldsValue
    } = props.form;

    // Only show error after a field is touched.
    const firstNameError = getFieldError('firstName');
    const lastNameError = getFieldError('lastName');
    const emailError = getFieldError('email');
    const passwordError = getFieldError('password');
    const confirmError = getFieldError('confirm');
    const locationError = getFieldError('location');
    const regionError = getFieldError('region');

    return (
        <div>
            {success
                ? <Message message={messageContent} title={messageTitle} />
                :
                <Form onSubmit={submit} hideRequiredMark>
                    <Card
                        className="login-box"
                        data-aos="fade-down"
                        data-aos-duration="1000"
                        data-aos-once="true"
                        bodyStyle={{
                            display: "flex",
                            flexFlow: "row wrap",
                            justifyContent: "center"
                        }}
                    >
                        <div className="left">
                            <h1>Become a Buddy</h1>

                            <div className="personal-info">
                                <FormItem
                                    validateStatus={firstNameError ? 'error' : ''}
                                    help={''}
                                >
                                    {getFieldDecorator('firstName', {
                                        validateTrigger: 'onBlur',
                                        rules: [
                                            { validator: validators.validateName }
                                        ],
                                        initialValue: ''
                                    })(
                                        <Tooltip placement="top" title={firstNameError} trigger="focus">
                                            <Input
                                                size="large"
                                                type="text"
                                                placeholder="First Name"
                                                prefix={<Icon type="user" />}
                                                required
                                            />
                                        </Tooltip>
                                    )}
                                </FormItem>
                                <FormItem
                                    validateStatus={lastNameError ? 'error' : ''}
                                    help={''}
                                >
                                    {getFieldDecorator('lastName', {
                                        validateTrigger: 'onBlur',
                                        rules: [
                                            { validator: validators.validateName }
                                        ],
                                        initialValue: ''
                                    })(
                                        <Tooltip placement="top" title={lastNameError} trigger="focus">
                                            <Input
                                                size="large"
                                                type="text"
                                                placeholder="Last Name"
                                                prefix={<Icon type="user" />}
                                                required
                                            />
                                        </Tooltip>
                                    )}
                                </FormItem>
                                <FormItem
                                    validateStatus={emailError ? 'error' : ''}
                                    help={''}
                                >
                                    {getFieldDecorator('email', {
                                        validateTrigger: 'onBlur',
                                        rules: [
                                            { validator: validators.validateEmail }
                                        ],
                                        initialValue: ''
                                    })(
                                        <Tooltip placement="top" title={emailError} trigger="focus">
                                            <Input
                                                size="large"
                                                type="email"
                                                placeholder="Email"
                                                prefix={<Icon type="mail" />}
                                                required
                                            />
                                        </Tooltip>
                                    )}
                                </FormItem>
                                <FormItem
                                    validateStatus={passwordError ? 'error' : ''}
                                    help={''}
                                >
                                    {getFieldDecorator('password', {
                                        validateTrigger: 'onBlur',
                                        rules: [
                                            { validator: validators.validateToNextPassword }
                                        ],
                                        initialValue: ''
                                    })(
                                        <Tooltip placement="top" title={passwordError} trigger="focus">
                                            <Input.Password
                                                size="large"
                                                type="password"
                                                placeholder="Password"
                                                prefix={<Icon type="lock" />}
                                                required
                                            />
                                        </Tooltip>
                                    )}
                                </FormItem>
                                <FormItem
                                    validateStatus={confirmError ? 'error' : ''}
                                    help={''}
                                >
                                    {getFieldDecorator('confirm', {
                                        rules: [
                                            { validator: validators.compareToFirstPassword(getFieldValue('password')) }
                                        ],
                                        initialValue: ''
                                    })(
                                        <Tooltip placement="top" title={confirmError} trigger="focus">
                                            <Input.Password
                                                size="large"
                                                type="password"
                                                placeholder="Confirm Password"
                                                prefix={<Icon type="lock" />}
                                                required
                                            />
                                        </Tooltip>
                                    )}
                                </FormItem>
                            </div>

                        </div>
                        <div className="right">
                            <FormContainer
                                getFieldDecorator={getFieldDecorator}
                                locationError={locationError}
                                regionError={regionError}
                                handleChange={handleChange}
                                countries={countries}
                                getFieldValue={getFieldValue}
                                getRegions={getRegions}
                                getFieldsError={getFieldsError}
                                setFieldsValue={setFieldsValue}
                                loading={loading}
                            />
                        </div>

                    </Card>
                </Form>
            }
        </div>
    );

}

export default Form.create()(SignUp);