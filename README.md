# BuddyBuilder

## Overview
BuddyBuilder is a social media network that aims to help people finding sport partners easier than ever.

## Installation
```
// Clone repository from github
git clone https://github.com/aboudicheng/buddybuilder.git

// Change directory
cd buddybuilder

// Install required dependencies
npm install

// Start development server on http://localhost:3000
npm start
```
## Tools & Libraries
#### Front-End:
- React
- Redux
- React router v4

#### Back-End
- Django
- PostgreSQL
- Memcached


## Creators
- Ping Cheng (Front-End)
- Abdurahman Atakishiyev (Back-End)
